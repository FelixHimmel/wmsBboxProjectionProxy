const express = require('express')
const app = express()
SphericalMercator = require('sphericalmercator'),
reprojectBoundingBox = require('reproject-bbox'),
sm = new SphericalMercator({ size: 256 }),
prequest = require('request').defaults({ encoding: null });


app.get('/:wmsurl/:targetEPSG/:layers/:z/:x/:y.png',
    function (request, reply) {
        var ibbox = sm.bbox(request.params.x, request.params.y, request.params.z, false, '4326');
        if(request.params.wmsurl.includes('1.3.')) {
            var org = ibbox;
            ibbox = [org[1],org[0],org[3],org[2]]
        }
        var bbox = reprojectBoundingBox({
            bbox: ibbox,
            from: 4326,
            to: parseInt(request.params.targetEPSG)
        });
        var url = `${decodeURIComponent(request.params.wmsurl)}&BBOX=${bbox.join(',')}&LAYERS=${decodeURIComponent(request.params.layers)}`;
        prequest.get(url, function (err, res, body) {
            reply.header('Cache-Control', 'public, max-age=10000')
            .header('Content-Type', 'image/png')
            .send(body)
        });
    }
);

app.listen(3026);